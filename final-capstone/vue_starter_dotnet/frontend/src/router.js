import Vue from 'vue'
import Router from 'vue-router'
import auth from './auth'
import Home from './Components/Home.vue'
import Login from './Components/Login.vue'
import Register from './Components/Register.vue'
import GetMyCards from './Components/GetMyCards.vue'
import GetAllCards from './Components/GetAllCards.vue'
import CreateCard from './Components/CreateCard.vue'
import ModifyCard from './Components/ModifyCard.vue'
import ViewCard from './Components/ViewCard.vue'
import TestZone from './Components/TestZone.vue'
import CreateDeck from './Components/CreateDeck.vue'
import GetAllDecks from './Components/GetAllDecks.vue'
import ViewDeck from './Components/ViewDeck.vue'
import ModifyDeck from './Components/ModifyDeck.vue'
import GetCardsByDeck from './Components/GetCardsByDeck.vue'
import CardList from './Components/CardList.vue'
import AvailableCardList from './Components/AvailableCardList.vue'
import AddCardsToDeck from './Components/AddOrRemoveCards.vue'
import StudySession from './Components/StudySession.vue'
import ViewCardAll from './Components/ViewCardAll.vue'

Vue.use(Router)

/**
 * The Vue Router is used to "direct" the browser to render a specific view component
 * inside of App.vue depending on the URL.
 *
 * It also is used to detect whether or not a route requires the user to have first authenticated.
 * If the user has not yet authenticated (and needs to) they are redirected to /login
 * If they have (or don't need to) they're allowed to go about their way.
 */

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/login",
      name: "login",
      component: Login,
      meta: {
        requiresAuth: false
      }
    },
    {
      path: "/register",
      name: "register",
      component: Register,
      meta: {
        requiresAuth: false
      }
    },
    {
      path: "/getmycards",
      name: "getmycards",
      component: GetMyCards,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/getallcards",
      name: "getallcards",
      component: GetAllCards,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/createcard",
      name: "createcard",
      component: CreateCard,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/modifycard/:cardId",
      name: "modifycard",
      component: ModifyCard,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/viewcard/cardId/:cardId",
      name: "viewcard",
      component: ViewCard,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/viewcardall/cardId/:cardId",
      name: "viewcardall",
      component: ViewCardAll,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/testzone",
      name: "testzone",
      component: TestZone,
      meta: {
        requiresAuth: true
      }
    },

    {
      path: "/createdeck",
      name: "createdeck",
      component: CreateDeck,
      meta: {
        requiresAuth: true
      }
    },

    {
      path: "/getalldecks",
      name: "getalldecks",
      component: GetAllDecks,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/viewdeck/deckId/:deckId",
      name: "viewdeck",
      component: ViewDeck,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/modifydeck/:deckId",
      name: "modifydeck",
      component: ModifyDeck,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/viewcard/cardId/:cardId",
      name: "viewcard",
      component: ViewCard,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/testzone",
      name: "testzone",
      component: TestZone,
      meta: {
        requiresAuth: true
      }
    },

    {
      path: "/createdeck",
      name: "createdeck",
      component: CreateDeck,
      meta: {
        requiresAuth: true
      }
    },

    {
      path: "/addorremovecards/deckId/:deckId",
      name: "addorremovecards",
      component: AddCardsToDeck,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/getcardsbydeck/deckId/:deckId",
      name: "getcardsbydeck",
      component: GetCardsByDeck,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/cardlist",
      name: "cardlist",
      component: CardList,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/availablecardlist",
      name: "availablecardlist",
      component: AvailableCardList,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/studysession/deckId/:deckId",
      name: "studysession",
      component: StudySession,
      meta: {
        requiresAuth: true
      }
    },
  ]
})

router.beforeEach((to, from, next) => {
  // Determine if the route requires Authentication
  const requiresAuth = to.matched.some(x => x.meta.requiresAuth);
  const user = auth.getUser();

  // If it does and they are not logged in, send the user to "/login"
  if (requiresAuth && !user) {
    next("/login");
  } else {
    // Else let them go to their next destination
    next();
  }
});

export default router;
